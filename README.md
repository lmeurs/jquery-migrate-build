Simple repo with full and minimal versions of jQuery Migrate, without version numbers in the filenames.

Direct download links to these files contain the version numbers in the filenames which makes it harder to load these files from Drupal without defining a Drupal library.